#!/usr/bin/env fish
if test -f /etc/default/keyboard
  sudo rm /etc/default/keyboard
end
sudo ln -s $PWD/etc/default/keyboard /etc/default/keyboard

if test -d $HOME/.config/fish
  rm -rf $HOME/.config/fish
end
ln -s $PWD/config/fish $HOME/.config/fish

if test -d $HOME/.config/nvim 
  rm -rf $HOME/.config/nvim
end
ln -s $PWD/config/nvim $HOME/.config/nvim

if test -f /usr/local/bin/toolbox
  sudo rm -rf /usr/local/bin/toolbox
end
sudo ln -s $HOME/.local/share/JetBrains/Toolbox/bin/jetbrains-toolbox /usr/local/bin/toolbox

if test -f $HOME/.ideavimrc
  rm $HOME/.ideavimrc
end
ln -s $PWD/.ideavimrc $HOME/.ideavimrc
