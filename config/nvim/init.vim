set number
set autoindent
set tabstop=2
set shiftwidth=2
set expandtab
set clipboard=unnamed
set hls

inoremap <silent> jj <ESC>
inoremap <silent> っｊ <ESC>
