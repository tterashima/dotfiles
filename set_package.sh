#!/usr/bin/env bash
LANG=C xdg-user-dirs-gtk-update
sudo apt update
sudo apt upgrade

# git settings
git config --global user.email "taira.terashima@gmail.com"
git config --global user.name "Taira Terashima"
git config --global core.editor "vi"

sudo apt install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg \
  lsb-release

# apt repository settings
sudo apt-add-repository ppa:fish-shell/release-3
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# apt packages install
sudo apt install -y curl \
  git-flow \
  neovim \
  fish \
  apt-transport-https \
  ca-certificates \
  software-properties-common \
  build-essential \
  libffi-dev \
  libssl-dev \
  zlib1g-dev \
  liblzma-dev \
  libbz2-dev \
  libreadline-dev \
  libsqlite3-dev \
  docker-ce
update-alternatives --config vi

sudo apt autoremove

# snap package install
sudo snap install chromium
sudo snap install slack --classic

# jetbrains ide install
TMP=/tmp
ARCHIVE=$TMP/jetbrains-toolbox.tar.gz
wget -cO $ARCHIVE "https://data.services.jetbrains.com/products/download?platform=linux&code=TBA"
tar xzvf $ARCHIVE -C $TMP
DIR=$(find /tmp -maxdepth 1 -type d -name jetbrains-toolbox-\* | head -n1)
cd $DIR
./jetbrains-toolbox
cd

# anyenv install
git clone https://github.com/anyenv/anyenv ~/.anyenv

# direnv install
wget -O direnv https://github.com/direnv/direnv/releases/download/v2.28.0/direnv.linux-amd64
chmod +x direnv
sudo chown root: direnv
sudo mv direnv /usr/local/bin
